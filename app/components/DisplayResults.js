import { parseInput } from "../utils/parseInput";
import { searchForWord } from "../utils/searchSolverUtils";

export default function DisplayResults({ inputData }) {
  const parsedData = parseInput(inputData);
  const { gridSize, gridRows, searchWords } = parsedData;

  const words = searchWords.map((word) => {
    return {
      word,
      coords: searchForWord(gridRows, word),
    };
  });
  const foundWords = words.filter((word) => word.coords.foundInGrid);

  return (
    <div className="text-black">
      <div className="flex items-start space-x-8">
        <div>
          <h2 className="text-lg font-semibold mb-4">Word Search Grid</h2>
          <div className="flex justify-start">
            <table className="border-collapse w-auto">
              <tbody>
                <tr>
                  <td className="py-2 px-3 text-center text-sm font-medium bg-transparent"></td>
                  {gridRows &&
                    gridRows.length > 0 &&
                    Array.from({ length: gridRows[0].length }).map(
                      (_, colIndex) => (
                        <td
                          key={colIndex}
                          className="py-2 px-3 text-center text-sm font-medium bg-transparent"
                        >
                          <span style={{ color: "black" }}>{colIndex}</span>
                        </td>
                      )
                    )}
                </tr>

                {gridRows &&
                  gridRows.map((row, rowIndex) => (
                    <tr key={rowIndex}>
                      <td className="py-2 px-3 text-center text-sm font-medium bg-transparent">
                        <span style={{ color: "black" }}>{rowIndex}</span>
                      </td>

                      {row.split("").map((char, colIndex) => (
                        <td
                          key={colIndex}
                          className={`py-2 px-3 text-center text-lg font-bold border bg-black`}
                        >
                          <span style={{ color: "white" }}>{char}</span>
                        </td>
                      ))}
                    </tr>
                  ))}
              </tbody>
            </table>
          </div>
        </div>
        <div className="mt-4">
          <h2 className="text-xl mb-2 text-right">Output:</h2>
          <pre className="bg-gray-800 p-2 rounded-md  w-60">
            <code className="text-white">
            {foundWords &&
              foundWords.map((word, index) => (
                <div key={index} className="text-right mb-1">
                  <span className="w-12 text-right">{`${word.word} ${word.coords.wordStartCoords[0]}:${word.coords.wordStartCoords[1]} ${word.coords.wordEndCoords[0]}:${word.coords.wordEndCoords[1]}`}</span>
                </div>
              ))}
            </code>
          </pre>
        </div>
      </div>
    </div>
  );
}
