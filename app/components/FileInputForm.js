import { useState } from "react";

export default function FileInputForm({ onFileChange, onSubmit }) {
    const [fileSelected, setFileSelected] = useState(false);

    const handleFileChange = (e) => {
        if (e.target.files.length > 0) {
            setFileSelected(true); // Set fileSelected to true if a file is chosen
        } else {
            setFileSelected(false); // Set fileSelected to false if no file is chosen
        }
        onFileChange(e); // Call the original onFileChange function
    }

    return (
      <form className="bg-white p-8 rounded-xl shadow-md space-y-4" data-testid="file-input-form">
        <div className="p-4 border border-gray-300 bg-gray-50 rounded-md space-y-2">
        <div className="p-4 border-gray-300 bg-gray-50 rounded-md space-y-2">
          <h3 className="font-semibold text-gray-700">Instructions:</h3>
          <p className="text-gray-600">Please input your word search in the following format:</p>
          <pre className="bg-gray-800 p-2 rounded-md  w-60">
            <code className="text-white">
{`3x3 //Grid Size
A B C //Grid
D E F //Grid
G H I //Grid
ABC // Search Word
AEI // Search Word
`}
            </code>
          </pre>
          <p className="text-gray-600">You may upload your plain text file with the word search via "Upload a File" option below. Click "Submit" To see results.</p>
        </div>
        </div>

        <div className="flex flex-col items-center space-y-4">
            <div className="flex items-center justify-center w-full">
              <label htmlFor="file-upload" className="text-gray-700 font-medium">Upload a file:</label>
              <input 
                id="file-upload"
                type="file" 
                onChange={handleFileChange}
                className="text-black border border-gray-300 p-2 rounded-md"
            />
            </div>
          {fileSelected && <button 
            onClick={onSubmit}
            className="mt-4 bg-blue-500 text-white px-6 py-2 rounded hover:bg-blue-600"
            type="button"
          >
            Submit
          </button>}
        </div>
      </form>
    );
}
