import { render, screen, fireEvent } from '@testing-library/react';
import FileInputForm from '../FileInputForm'; // Adjust the path accordingly

describe('FileInputForm', () => {
    const mockOnFileChange = jest.fn();
    const mockOnSubmit = jest.fn();

    beforeEach(() => {
        render(
            <FileInputForm 
                onFileChange={mockOnFileChange}
                onSubmit={mockOnSubmit}
            />
        );
    });

    test('Invoke onFileChange when a file is selected', () => {
        const file = new File(['test content'], 'test.txt', { type: 'text/plain' });
        fireEvent.change(screen.getByLabelText('Upload a file:'), { target: { files: [file] } });
        expect(mockOnFileChange).toHaveBeenCalled();
    });

    test('Set fileSelected to false if the file input has been cleared', () => {
        const fileInput = screen.getByLabelText("Upload a file:");
        fireEvent.change(fileInput, { target: { files: ['test.txt'] } });
        expect(screen.getByRole('button', { name: 'Submit' })).toBeInTheDocument();

        fireEvent.change(fileInput, { target: { files: [] } });
        expect(screen.queryByRole('button', { name: 'Submit' })).not.toBeInTheDocument();
    });

    test('Calls onSubmit when submit button is clicked', () => {
        const fileInput = screen.getByLabelText("Upload a file:");
        fireEvent.change(fileInput, { target: { files: ['test.txt'] } });
        fireEvent.click(screen.getByRole('button', { name: 'Submit' }));
        expect(mockOnSubmit).toHaveBeenCalled();
    });
});
