import React from 'react';
import { render } from '@testing-library/react';
import DisplayResults from '../DisplayResults';

describe('<DisplayResults />', () => {
    const mockInputData = `
    3x3
    ABC
    DEF
    GHI
    ABC
    DEF
    `;

    it('renders without crashing', () => {
        render(<DisplayResults inputData={mockInputData} />);
    });

    it('renders the correct number of rows and columns', () => {
        const { getAllByRole } = render(<DisplayResults inputData={mockInputData} />);
        const rows = getAllByRole('row');
        expect(rows).toHaveLength(4); // 3 rows + 1 for the header

        const cells = getAllByRole('cell');
        expect(cells).toHaveLength(16); // 3x3 grid + 3 for the row headers
    });

    it('renders the correct words in the "Found Words" section', () => {
        const { getByText } = render(<DisplayResults inputData={mockInputData} />);
        expect(getByText('ABC 0:0 0:2')).toBeInTheDocument();
        expect(getByText('DEF 1:0 1:2')).toBeInTheDocument();
    });

    // Add more tests as needed
});
