"use client";
import FileInputForm from "./components/FileInputForm";
import React from "react";
import { useCallback, useState } from "react";
import Image from "next/image";
import { handleFileUpload as handleFileUploadHelper } from "./utils/handleFileUpload";
import DisplayResults from "./components/DisplayResults";

export default function Home() {
  const [inputData, setInputData] = useState(null);
  const [tempData, setTempData] = useState(null);

  const handleFileUpload = useCallback((event) => {
    handleFileUploadHelper(event, setTempData);
  }, []);

  const handleSubmit = useCallback(
    (event) => {
      event.preventDefault();
      setInputData(tempData);
      setTempData(null);
    },
    [tempData]
  );

  return (
    <main className="flex flex-col min-h-screen items-center justify-center p-24 space-y-8">
      <div className="p-4 rounded-lg shadow-md">
        <Image
          src="/images/logo-no-background.png"
          alt="Crossword Key Creator"
          width={400}
          height={100}
        />
      </div>
      {inputData ? (
        <>
          <div className="bg-white p-8 rounded-xl shadow-md">
            <DisplayResults inputData={inputData} />
          </div>
          <button
            className="text-white bg-blue-500 hover:bg-blue-600 px-4 py-2 rounded"
            onClick={() => setInputData(null)}
          >
            Reset
          </button>
        </>
      ) : (
        <FileInputForm
          onFileChange={handleFileUpload}
          onSubmit={handleSubmit}
        />
      )}
    </main>
  );
}
