export function handleFileUpload(event, setTempData) {
    const file = event.target.files[0];
    if (file) {
      const reader = new FileReader();
      reader.onload = function(e) {
        setTempData(e.target.result);
      }
      reader.readAsText(file);
    }
}