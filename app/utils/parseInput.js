/**
 * Parses the input string and extracts relevant data.
 * @param {string} input - The input string to parse.
 * @returns {object} - An object containing the parsed data.
 */
export function parseInput(input) {
    if (input) {
      const lines = input.trim().split('\n');
      const [rows, cols] = lines[0].split('x').map(Number);
      const gridRows = [];
  
      // Extract grid rows
      for (let i = 1; i <= rows; i++) {
        if (lines[i]) {
          const row = lines[i].split(' ').join('').toUpperCase();
          gridRows.push(row);
        }
      }
      
      // Extract search words
      const searchWords = lines.slice(rows + 1).map(word => word.replace(/\s+/g, '').toUpperCase());
  
      return {
        gridSize: { rows, cols },
        gridRows,
        searchWords,
      };
    } else {
      // Return empty data if input is falsy
      return {
        gridSize: {},
        gridRows: [],
        searchWords: [],
      };
    }
  }