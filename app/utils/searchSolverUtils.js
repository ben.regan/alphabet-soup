/**
 * These utility functions are used to create search strings from a grid of letters and conduct a
 * search for a specific word within that grid. The initial grid is made from each row of letters
 * from top to bottom delimited by line breaks. The search will check rows, columns, and diagonals
 * for each word, both forwards and in reverse. The end result should include the word that was
 * found, and it's coordinates within the grid
 */

/**
 * Generates search strings from the grid.
 * @param {string[][]} grid - The Array of grid rows to generate search strings from.
 * @returns {object} - An object containing the generated search strings.
 */
export function generateSearchStrings(grid) {
    const rows = grid;
    const cols = [];
    const diagonals = [];

    if (grid && grid.length > 0) {
        // Generate columns
        for (let columnIndex = 0; columnIndex < grid[0].length; columnIndex++) {
            let columnString = '';
            for (let rowIndex = 0; rowIndex < grid.length; rowIndex++) {
                columnString += grid[rowIndex][columnIndex];
            }
            cols.push(columnString);
        }

        // Generate major diagonals (from top-left to down-right).
        // Start from each row of the first column and move diagonally to the bottom-right.
        // Each diagonal is represented by its start and end coordinates.
        for (let startRow = 0; startRow < grid.length; startRow++) {
            let diagonalString = '';
            let start = [startRow, 0];
            let end;
            for (let offset = 0; startRow + offset < grid.length && offset < grid[0].length; offset++) {
                diagonalString += grid[startRow + offset][offset];
                end = [startRow + offset, offset];
            }
            if (diagonalString.length > 1) diagonals.push({ string: diagonalString, start, end, direction: 'top-left-down-right' });
        }

        // Continue generating major diagonals, but this time starting from the top row
        // and moving from left to right for each column, then moving diagonally to the bottom-right.
        for (let startCol = 1; startCol < grid[0].length; startCol++) {
            let diagonalString = '';
            let start = [0, startCol];
            let end;
            for (let offset = 0; startCol + offset < grid[0].length && offset < grid.length; offset++) {
                diagonalString += grid[offset][startCol + offset];
                end = [offset, startCol + offset];
            }
            if (diagonalString.length > 1) diagonals.push({ string: diagonalString, start, end, direction: 'top-left-down-right' });
        }

        // Generate minor diagonals (from top-right to down-left).
        // Start from each row of the last column and move diagonally to the bottom-left.
        for (let startRow = 0; startRow < grid.length; startRow++) {
            let diagonalString = '';
            let start = [startRow, grid[0].length - 1];
            let end;
            for (let offset = 0; startRow + offset < grid.length && offset < grid[0].length; offset++) {
                diagonalString += grid[startRow + offset][grid[0].length - 1 - offset];
                end = [startRow + offset, grid[0].length - 1 - offset];
            }
            if (diagonalString.length > 1) diagonals.push({ string: diagonalString, start, end, direction: 'top-right-down-left' });
        }
        
        // Continue generating minor diagonals, but this time starting from the top row
        // and moving from right to left for each column, then moving diagonally to the bottom-left.
        for (let startCol = grid[0].length - 2; startCol >= 0; startCol--) {
            let diagonalString = '';
            let start = [0, startCol];
            let end;
            for (let offset = 0; startCol - offset >= 0 && offset < grid.length; offset++) {
                diagonalString += grid[offset][startCol - offset];
                end = [offset, startCol - offset];
            }
            if (diagonalString.length > 1) diagonals.push({ string: diagonalString, start, end, direction: 'top-right-down-left' });
        }
    }

    return { rows, cols, diagonals };
}

/**
 * Searches for a word in the grid.
 * @param {string[][]} grid - The grid to search in.
 * @param {string} word - The word to search for.
 * @returns {object|null} - An object containing the search result, or null if the word is not found.
 */
export function searchForWord(grid, word) {
    const { rows, cols, diagonals } = generateSearchStrings(grid);
    const reversedWord = word.split('').reverse().join('');
    
   /**
   * Searches for a word in an array of strings or objects.
   * @param {Array} arrayToSearch - The array to search in.
   * @param {string} targetWord - The word to search for.
   * @param {string} type - The type of search (rows, cols, or diag).
   * @returns {object|null} - An object containing the search result, or null if the word is not found.
   */
    const findWordInArray = (arrayToSearch = [], targetWord = '', type = '') => {
        for (let index = 0; index < arrayToSearch.length; index++) {
            const currentString = typeof arrayToSearch[index] === 'string' ? arrayToSearch[index] : arrayToSearch[index].string;

            if (currentString) {
                const startIndex = currentString.indexOf(targetWord);
                if (startIndex !== -1) {
                    let start, end;
                    switch (type) {
                        case 'rows':
                            start = [index, startIndex];
                            end = [index, startIndex + targetWord.length - 1];
                            break;
                        case 'cols':
                            start = [startIndex, index];
                            end = [startIndex + targetWord.length - 1, index];
                            break;
                        default:
                            start = arrayToSearch[index].start;
                            end = arrayToSearch[index].end;
                            break;
                    }
                    return {
                        startIndex: startIndex,
                        endIndex: startIndex + targetWord.length - 1,
                        start: start,
                        end: end,
                        direction: arrayToSearch[index].direction || ''
                    };
                }
            }
        }
        return null;
    };
   
   /**
   * Formats the search result for the type of search.
   * @param {object|null} result - The search result.
   * @param {string} type - The type of search (rows, cols, or diag).
   * @param {boolean} isReversed - Indicates if the word is reversed.
   * @returns {object|null} - The formatted search result.
   */
    const formatResult = (result, type, isReversed) => {
        if (!result) return null;
    
        let wordStartCoords, wordEndCoords;
    
        if (type === 'rows') {
            if (!isReversed) {
                wordStartCoords = [result.start[0], result.startIndex];
                wordEndCoords = [result.end[0], result.endIndex];
            } else {
                wordStartCoords = [result.end[0], result.endIndex];
                wordEndCoords = [result.start[0], result.startIndex];
            }
        } else if (type === 'cols') {
            if (!isReversed) {
                wordStartCoords = [result.startIndex, result.start[1]];
                wordEndCoords = [result.endIndex, result.end[1]];
            } else {
                wordStartCoords = [result.endIndex, result.end[1]];
                wordEndCoords = [result.startIndex, result.start[1]];
            }
        } else if (result.direction === "top-left-down-right") {
            if (!isReversed) {
                wordStartCoords = [result.start[0], result.start[1]];
                wordEndCoords = [result.start[0] + word.length - 1, result.start[1] + word.length - 1];
            } else {
                wordStartCoords = [result.start[0] + word.length - 1, result.start[1] + word.length - 1];
                wordEndCoords = [result.start[0], result.start[1]];
            }
        } else if (result.direction === "top-right-down-left") {
            if (!isReversed) {
                wordStartCoords = [result.start[0], result.start[1]];
                wordEndCoords = [result.start[0] + word.length - 1, result.start[1] - word.length + 1];
            } else {
                wordStartCoords = [result.start[0] + word.length - 1, result.start[1] - word.length + 1];
                wordEndCoords = [result.start[0], result.start[1]];
            }
        }
    
        return {
            word: word,
            foundInGrid: true,
            wordStartCoords: wordStartCoords,
            wordEndCoords: wordEndCoords
        };
    };
    
    

    // Check each row forward and reverse
    let searchResult = findWordInArray(rows, word, 'rows');
    if (searchResult) return formatResult(searchResult, 'rows', false);

    searchResult = findWordInArray(rows, reversedWord, 'rows');
    if (searchResult) return formatResult(searchResult, 'rows', true);

    // Check each column forward and reverse
    searchResult = findWordInArray(cols, word, 'cols');
    if (searchResult) return formatResult(searchResult, 'cols', false);

    searchResult = findWordInArray(cols, reversedWord, 'cols');
    if (searchResult) return formatResult(searchResult, 'cols', true);

    // Check diagonals forward and reverse
    searchResult = findWordInArray(diagonals, word, 'diag');
    if (searchResult) return formatResult(searchResult, 'diag', false);

    searchResult = findWordInArray(diagonals, reversedWord, 'diag');
    if (searchResult) return formatResult(searchResult, 'diag', true);


    return {
        word: word,
        foundInGrid: false,
        wordStartCoords: null,
        wordEndCoords: null
    };
}

