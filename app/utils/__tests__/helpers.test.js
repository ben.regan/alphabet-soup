import { handleFileUpload } from '../handleFileUpload';
import { parseInput } from '../parseInput';
import { generateSearchStrings, searchForWord } from '../searchSolverUtils';

describe('handleFileUpload', () => {
    let mockFile;
    let mockEvent;
    let setTempData;
  
    beforeEach(() => {
      
      global.FileReader = jest.fn(function() {
        this.readAsText = jest.fn();
      });
  
      
      mockFile = new Blob(['test content'], { type: 'text/plain' });
      mockEvent = {
        target: {
          files: [mockFile],
        },
      };
  
      
      setTempData = jest.fn();
    });
  
    test('reads the file content and sets the temp data', () => {
      handleFileUpload(mockEvent, setTempData);
  
      
      expect(FileReader).toHaveBeenCalled();
      expect(global.FileReader.mock.instances[0].readAsText).toHaveBeenCalledWith(mockFile);
  
      
      global.FileReader.mock.instances[0].onload({
        target: {
          result: 'test content',
        },
      });
  
      
      expect(setTempData).toHaveBeenCalledWith('test content');
    });
    test('does nothing if no file is provided', () => {
        
        const mockEventWithNoFiles = {
          target: {
            files: [],
          },
        };
    
        handleFileUpload(mockEventWithNoFiles, setTempData);
    
        
        expect(FileReader).not.toHaveBeenCalled();
    
        
        expect(setTempData).not.toHaveBeenCalled();
      });
  });

  describe('parseInput', () => {

    test('Parses input in to the needed segments', () => {
        const input = `2x2
        A B
        C D
        WORD1
        WORD2`;

        const result = parseInput(input);
      
        expect(result).toEqual({
            gridSize: { rows: 2, cols: 2 },
            gridRows: ['AB', 'CD'],
            searchWords: ['WORD1', 'WORD2']
        });
    });

    test('Parsed input has the expected number of rows', () => {
      const input1 =
      `2x2
      A B
      C D
      WORD1
      WORD2`;

      const input2 =
      `5x6
      H A S D F H
      G E Y B H L
      J K L Z X B
      C V B L N R
      G O O D O C
      HELLO
      GOOD
      BYE`;

      const result1 = parseInput(input1);
      const result2 = parseInput(input2);
      
      expect(result1.gridRows.length).toEqual(2);
      expect(result2.gridRows.length).toEqual(5);
      
  });

    test('Is empty for empty input', () => {
        const result = parseInput('');

        expect(result).toEqual({
            gridSize: {},
            gridRows: [],
            searchWords: []
        });
    });

    test('Removes extra spaces', () => {
        const input = `2x2
        A B
        C D
        WORD1
        WORD TWO`;

        const result = parseInput(input);

        expect(result).toEqual({
            gridSize: { rows: 2, cols: 2 },
            gridRows: ['AB', 'CD'],
            searchWords: ['WORD1', 'WORDTWO']
        });
    });

});

describe('generateSearchStrings', () => {
    test('Returns empty for an empty grid', () => {
        const grid = [];
        const result = generateSearchStrings(grid);
        expect(result.rows).toEqual([]);
        expect(result.cols).toEqual([]);
        expect(result.diagonals).toEqual([]);
    });

    test('Converts grid rows in to anticipated strings of rows, columns and diagonals', () => {
        const grid = [
            'AB',
            'CD'
        ];
        const result = generateSearchStrings(grid);
        expect(result.rows).toEqual(grid);
        expect(result.cols).toEqual(['AC', 'BD']);
        expect(result.diagonals).toEqual([
            {string: 'AD', start: [0, 0], end: [1, 1], direction: 'top-left-down-right' },
            {string: 'BC', start: [0, 1], end: [1, 0], direction: 'top-right-down-left' }

        ]);
    });


    test('Correctly processes larger grid', () => {
      const grid = [
        'HASDF',
        'GEYBH',
        'JKLZX',
        'CVBLN',
        'GOODO'
      ];
      const expectedCols = ['HGJCG', 'AEKVO', 'SYLBO', 'DBZLD', 'FHXNO'];
      const expectedDiagonals = [
        {string: 'HELLO', start: [0, 0], end: [4, 4], direction: 'top-left-down-right' },
        {string: 'GKBD', start: [1, 0], end: [4, 3], direction: 'top-left-down-right' },
        {string: 'JVO', start: [2, 0], end: [4, 2], direction: 'top-left-down-right' },
        {string: 'CO', start: [3, 0], end: [4, 1], direction: 'top-left-down-right' },
        {string: 'AYZN', start: [0, 1], end: [3, 4], direction: 'top-left-down-right' },
        {string: 'SBX', start: [0, 2], end: [2, 4], direction: 'top-left-down-right' },
        {string: 'FBLVG', start: [0, 4], end: [4, 0], direction: 'top-right-down-left' },
        {string: 'HZBO', start: [1, 4], end: [4, 1], direction: 'top-right-down-left' },
        {string: 'XLO', start: [2, 4], end: [4, 2], direction: 'top-right-down-left' },
        {string: 'DYKC', start: [0, 3], end: [3, 0], direction: 'top-right-down-left' },
        {string: 'SEJ', start: [0, 2], end: [2, 0], direction: 'top-right-down-left' },
        {string: 'AG', start: [0, 1], end: [1, 0], direction: 'top-right-down-left' }
      ];

      const result = generateSearchStrings(grid);
      expect(result.rows).toEqual(grid);
      expect(result.cols).toEqual(expectedCols);
      expect(result.diagonals).toEqual(expect.arrayContaining(expectedDiagonals));
  });
});

describe('searchForWord', () => {

  const grid = [
      'HASDF',
      'TEYBH',
      'OELZX',
      'OVBLN',
      'GOODO'
  ];

  test('Finds word in a row', () => {
      const result = searchForWord(grid, 'GOOD');

      expect(result.wordStartCoords).toEqual([4, 0]);
      expect(result.wordEndCoords).toEqual([4, 3]);
  });

  test('Finds reverse word in a row', () => {
    const result = searchForWord(grid, 'BYE');


      expect(result.wordStartCoords).toEqual([1, 3]);
      expect(result.wordEndCoords).toEqual([1, 1]);
  });

    test('Finds word in a column', () => {
        const result = searchForWord(grid, 'TOO');

        expect(result.wordStartCoords).toEqual([1, 0]);
        expect(result.wordEndCoords).toEqual([3, 0]);
    });

    test('Finds word that spans a full column', () => {
      const result = searchForWord(grid, 'FHXNO');

      expect(result.wordStartCoords).toEqual([0, 4]);
      expect(result.wordEndCoords).toEqual([4, 4]);
  });

  test('Finds word that spans a full column in the middle of the grid', () => {
    const result = searchForWord(grid, 'AEEVO');

    expect(result.wordStartCoords).toEqual([0, 1]);
    expect(result.wordEndCoords).toEqual([4, 1]);
  });

  test('Finds word that spans a full column in reverse', () => {
    const result = searchForWord(grid, 'ONXHF');

    expect(result.wordStartCoords).toEqual([4, 4]);
    expect(result.wordEndCoords).toEqual([0, 4]);
  });

  test('Finds word that spans a full column in the middle of the grid in reverse', () => {
    const result = searchForWord(grid, 'OVEEA');
  expect(result.wordStartCoords).toEqual([4, 1]);
  expect(result.wordEndCoords).toEqual([0, 1]);
  });

  test('Finds reverse word in a column', () => {
      const result = searchForWord(grid, 'GOOT');

      expect(result.wordStartCoords).toEqual([4, 0]);
      expect(result.wordEndCoords).toEqual([1, 0]);
  });

  test('Finds word in a Top Left Down Right diagonal', () => {
      const result = searchForWord(grid, 'HELLO');
      expect(result.wordStartCoords).toEqual([0, 0]);
      expect(result.wordEndCoords).toEqual([4, 4]);
  });

  test('Finds reverse word in a Top Left Down Right diagonal', () => {
      const result = searchForWord(grid, 'OLLEH');
      expect(result.wordStartCoords).toEqual([4, 4]);
      expect(result.wordEndCoords).toEqual([0, 0]);
  });

  test('Finds word in a Top Right Down Left diagonal', () => {
      const result = searchForWord(grid, 'DYE');
  
      expect(result.wordStartCoords).toEqual([0, 3]);
      expect(result.wordEndCoords).toEqual([2, 1]);
  });

  test('Finds reverse word in a Top Right Down Left diagonal', () => {
      const result = searchForWord(grid, 'EYD');
  
      expect(result.wordStartCoords).toEqual([2, 1]);
      expect(result.wordEndCoords).toEqual([0, 3]);
  });

  test('returns null if word is not found', () => {
      const result = searchForWord(grid, 'NOTFOUND');
      expect(result.foundInGrid).toBe(false);
  });
});