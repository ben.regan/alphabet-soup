module.exports = {
    testPathIgnorePatterns: ['/node_modules/', '/.next/'],
    setupFilesAfterEnv: ['<rootDir>/setupTests.js'],
    testEnvironment: 'jsdom',
    moduleNameMapper: {
      '^@/components/(.*)$': '<rootDir>/app/components/$1',
      '^@/pages/(.*)$': '<rootDir>/app/pages/$1',
    },
    transform: {
      '^.+\\.(t|j)sx?$': '@swc/jest'
    },
    reporters: [
        "default",
        ["./node_modules/jest-html-reporter", {
            "pageTitle": "Test Report"
        }]
    ],
    collectCoverage: true,
    coverageReporters: ["html", "text"]
  };