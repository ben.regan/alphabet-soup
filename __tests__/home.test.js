import { render, screen, fireEvent, waitFor } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { handleFileUpload as handleFileUploadHelper } from '../app/utils/handleFileUpload';
import { parseInput } from '../app/utils/parseInput';

import Home from '../app/page';

// Mock the next/image component
jest.mock('next/image', () => ({
    __esModule: true,
    default: (props) => {
      return <img {...props} />;
    },
  }));
  jest.mock('../app/utils/handleFileUpload', () => ({
    handleFileUpload: jest.fn((event, setFunction) => {
        setFunction('file content');
    }),
}));

  
afterEach(() => {
  jest.clearAllMocks();
});

describe('Home', () => {

    test('renders the FileInputForm component initially', async () => {
      render(<Home />);
      const fileInputForm = await screen.findByTestId('file-input-form');
      expect(fileInputForm).toBeInTheDocument();
    });

    test('handleFileUpload is called when a file is selected', async () => {
        render(<Home />);

        await waitFor(() => {
        const fileInput = screen.getByLabelText('Upload a file:');
        
        const file = new File(['file content'], 'file.txt', { type: 'text/plain' });

        fireEvent.change(fileInput, { target: { files: [file] } });
    
        expect(handleFileUploadHelper).toHaveBeenCalled();
        });
    });

    test('Resets the input form', async () => {
        render(<Home />);
        const fileInput = screen.getByLabelText('Upload a file:');
        
        const file = new File(['file content'], 'file.txt', { type: 'text/plain' });
        
        fireEvent.change(fileInput, { target: { files: [file] } });
        
        const submitButton = screen.getByText('Submit');
        userEvent.click(submitButton);
        
        await waitFor(() => {

            expect(screen.queryByText('file content')).not.toBeInTheDocument();
        });
        
        
        const resetButton = await screen.findByText('Reset');
        userEvent.click(resetButton);
        
        await waitFor(async () => {
            const fileInputForm = await screen.findByTestId('file-input-form');
            expect(fileInputForm).toBeInTheDocument();
        });
    });
  });